const AppError = require('./AppError');
class ValidationError extends AppError {

  constructor(msg = 'Validation Error.') {
    super(msg, 'ValidationError');
    this.status = 400;
    this.type = 'validation';
  }
}

module.exports = ValidationError;
