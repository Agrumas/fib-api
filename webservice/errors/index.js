const AppError = require('./AppError');
const ValidationError = require('./ValidationError');

module.exports = {
    AppError,
    ValidationError
};
