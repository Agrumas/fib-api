const path = require('path');

class AppError extends Error {

    constructor(msg, id, name = null) {
        super(msg, id);
        this.status = 400;
        this.name = name || this.constructor.name;
    }
}

Reflect.defineProperty(Error.prototype, 'toJSON', {
    configurable: true,
    value() {
        const alt = {};
        Reflect.getOwnPropertyNames(this).
            forEach((key) => {
                if (key === 'stack') {
                    alt.stacktrace = formatStack(this.stack);
                    return;
                }
                alt[key] = this[key];
            });
        return alt;
    }
});

module.exports = AppError;

const cwd = `${path.resolve(process.cwd(), '../')}\\`;
const cwdRegexp = new RegExp(escapeRegexp(cwd), 'g');

function formatStack(stack) {
    return stack.replace(cwdRegexp, '').
        split('\n');
}

function escapeRegexp(s) {
    return s.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
}
