const {wrap} = require('./responseFormatter');

class Router {
	constructor(router) {
		this.router = router;
	}

	/**
	 *
	 * @param path
	 * @param handler
	 * @return {Router}
	 */
	get(path, handler) {
		this.router.get(path, wrap(handler));
		return this;
	}

	post(path, func) {
		this.router.post(path, wrap(func));
		return this;
	}
}

module.exports = Router;