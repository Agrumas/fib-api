const {Transform} = require('stream');
const asyncIteratorToStream = require('async-iterator-to-stream');

function wrap(handler) {
    return async (ctx, next) => {
        try {
            const response = await handler(ctx, next);
            if (response && typeof response.next === 'function') {

                /*
                 * Content-type octet-stream is returned only when explicitly asked
                 * otherwise plain text is used to make content visible in browser by default
                 */
                if (ctx.request.type !== 'application/octet-stream') {
                    ctx.type = 'text/plain; charset=utf-8';
                }
                ctx.body = asyncIteratorToStream.obj(response).
                    pipe(transform());
            } else if (!ctx.body && response !== undefined) {
                ctx.body = response;
            }
        } catch (err) {
            ctx.status = err.status || 500;
            ctx.body = err.message;
        }
    };
}

function transform() {
    let first = true;

    return new Transform({
        objectMode: true,
        transform(value, _, done) {
            if (first) {
                done(null, `${value}`);
                first = false;
            } else {
                done(null, ` ${value}`);
            }
        }
    });
}


module.exports.wrap = wrap;