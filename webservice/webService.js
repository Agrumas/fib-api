const path = require('path');
const Koa = require('koa');
const KoaRouter = require('koa-router');
const Router = require('./router');

class WebService {
	constructor(modules = []) {
		this.modules = modules;
	}

	start(port) {
		if (this.server) {
			throw new Error('WebService is already running');
		}
		this.app = new Koa();
		const koaRouter = new KoaRouter();
		this.router = new Router(koaRouter);

		this.loadModules();
		this.app
			.use(koaRouter.routes())
			.use(koaRouter.allowedMethods());

		this.server = this.app.listen(port);
		console.log(`Modules loaded. Webservice url: http://127.0.0.1:${port}`);
		return this.server;
	}

	/**
	 * @protected
	 */
	loadModules() {
		this.modules.forEach((name) => {
			process.stdout.write(`Loading ${name}...`);
			try {
                // eslint-disable-next-line global-require
				const module = require(path.resolve(process.cwd(), `./modules/${name}`));
				if (!module.init) {
					throw new Error('Not a module');
				}
				module.init(this.router);
				console.log('ok');
			} catch (e) {
				console.error('failed', e);
			}
		});
	}
}

module.exports = WebService;