/**
 *
 * @param {Router} router
 */
function init(router) {
	router.get('/test', async () => {
		return 'Hello world';
	});
}

module.exports.init = init;