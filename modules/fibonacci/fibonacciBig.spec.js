const {expect} = require('chai');
const Big = require('big.js');
const fibonacciBig = require('./fibonacciBig');
describe('Fibonacci BigInt generator', function () {

	it('returns requested number of items', () => {
		const it = fibonacciBig(5);
		const result = [...it];
		expect(result).to.have.lengthOf(5);
	});

	it('returns valid fibonacci sequence', () => {
		const it = fibonacciBig(5);
		const result = [...it];
		expect(result.map((i) => Number(`${i}`))).to.eql([0, 1, 1, 2, 3]);
	});

	it('returns valid fibonacci sequence when numbers are higher than 80', () => {
		const it = fibonacciBig(80);
		const result = [...it];
		const last = result[result.length - 1];
		const valid = '14472334024676221';
		expect(last.eq(Big(valid)), `Last number to be ${valid}`).to.be.true;
	});


	it('returns correct number of results when sequence values are larger than Number.MAX_VALUE', function () {
		this.slow(1000);
		const it = fibonacciBig(14780);
		const result = [...it];
		expect(result).to.have.lengthOf(14780);
	});


	it('returns empty array when 0 is requested', () => {
		const it = fibonacciBig(0);
		const result = [...it];
		expect(result).to.eql([]);
	});

	it('returns empty array negative is requested', () => {
		const it = fibonacciBig(-2);
		const result = [...it];
		expect(result).to.eql([]);
	});
});