function* fibonacci(n) {
	const infinite = n === undefined;
	let current = 0;
	let next = 1;

	while (isFinite(current) && (infinite || n-- > 0)) {
		yield current;
		[current, next] = [next, current + next];
	}
}

module.exports = fibonacci;