const Big = require('big.js');
function* fibonacci(n) {
	const infinite = n === undefined;
	let current = Big(0);
	let next = Big(1);

	while (infinite || n-- > 0) {
		yield current;
		[current, next] = [next, current.plus(next)];
	}
}

module.exports = fibonacci;