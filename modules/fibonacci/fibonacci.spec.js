const {expect} = require('chai');
const fibonacci = require('./fibonacci');
describe('Fibonacci sequence generator', () => {

	it('returns requested number of items', async () => {
		const it = fibonacci(5);
		const result = [...it];
		expect(result).to.have.lengthOf(5);
	});

	it('returns valid fibonacci sequence', async () => {
		const it = fibonacci(5);
		const result = [...it];
		expect(result).to.eql([0, 1, 1, 2, 3]);
	});


	it('returns empty array when 0 is requested', async () => {
		const it = fibonacci(0);
		const result = [...it];
		expect(result).to.eql([]);
	});

	it('returns empty array negative is requested', async () => {
		const it = fibonacci(-2);
		const result = [...it];
		expect(result).to.eql([]);
	});
});