const {expect} = require('chai');
const fibonacciBig = require('./fibonacciBig');
const fibonacci = require('./fibonacci');
describe('Fibonacci implementation comparison', function () {

	describe('validity', function () {
		it('returns the same numbers', () => {
			const n = 79;
			const itBig = fibonacciBig(n);
			const it = fibonacci(n);
			const result = [...it];
			const resultBig = [...itBig];
			resultBig.forEach((fib, idx) => {
				expect(fib.eq(result[idx]), `${idx + 1}-n number doesn't match Big: ${fib} JS: ${result[idx]}`).to.be.true;
			});
		});
	});

	describe('performance', function () {
		this.slow(3000);
		this.timeout(5000);
		it('native implementation is faster', () => {
			const n = 79;
			const reps = 40000;
			const bigDuration = perf(reps, () => {
				const itBig = fibonacciBig(n);
				let res = itBig.next();
				while (!res.done) {
					res = itBig.next();
				}
			});
			const nativeDuration = perf(reps, () => {
				const it = fibonacci(n);
				let res = it.next();
				while (!res.done) {
					res = it.next();
				}
			});

			after(() => {
				console.info('\tNative is %f faster. Native: %dms Big: %dms', (bigDuration / nativeDuration).toFixed(2), (nativeDuration).toFixed(2), (bigDuration).toFixed(2));
			});
		});
	});
});

function perf(reps, func) {
	let i = 0;
	const start = process.hrtime();
	while (i++ < reps) {
		func();
	}
	const end = process.hrtime();
	return (end[1] - start[1]) / 1000000 + (end[0] - start[0]) * 1000;
}