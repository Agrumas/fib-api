const fibonacciHandler = require('./fibonacciHandler');

/**
 *
 * @param {Router} router
 */
function init(router) {
	router.get('/fibonacci', fibonacciHandler);
}

module.exports.init = init;