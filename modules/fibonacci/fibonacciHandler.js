const {ValidationError} = require('../../webservice/errors');
const fibonacci = require('./fibonacci');
const fibonacciBig = require('./fibonacciBig');

function fibonacciHandler({request}) {
	const {n} = request.query;
	const number = Number(n);
	if (isNaN(number) || parseInt(number) != n) {
		throw new ValidationError('Number is not provided or it is invalid');
	}
	if (number < 0) {
		throw new ValidationError('Number must be positive');
	}

	/**
	 * 80th number is incorrect when sequence is generated using native numbers
	 */
	if (number >= 79) {
		return fibonacciBig(number);
	}
	return fibonacci(number);
}

module.exports = fibonacciHandler;
