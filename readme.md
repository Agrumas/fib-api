# Fibonacci sequence generator
This webservice computes and returns the first n Fibonacci numbers. Results are streamed without waiting complete sequence to be calculated, therefore TTFB and memory usage are lower.

## Requirements
* [NodeJS](https://nodejs.org/en/). Tested with version 8, however should be compatible with 8+. 

## Installation
````
npm install
````

## Usage
Start server
````
npm start
````
Webservice url: `http://127.0.0.1:3000/fibonacci?n={number}`. Example:
```
curl -X GET http://127.0.0.1:3000/fibonacci?n=5
```

Static analysis and tests(unit, e2e) can be executed using:
````
npm run validate
````

## Testing
Testing is done using Mocha and ChaiJS assert library. SuperTest is used for E2E tests.

### Unit
Unit tests can be executed via:
````
npm test
````

One performance test is included to compare native and BigInt implementations.

### End-to-end
End-to-end tests can be executed via:
````
npm run e2e
````
Command starts webservice and executes tests.

### Static analysis
ESLint is configured to avoid common mistakes and keep style consistent. Command:
````
npm run lint
````


## Notes

1. ECMAScript does not support BigInt, so third-party library is used to work with BigInts and calculate sequences longer than 79 numbers. Shorter sequences are calculated using native numbers to increase performance. 
1. Calculation of fibonacci sequence is CPU bound task therefore Node.js isn't optimal choice. However generated data can be easily cached.
