const WebService = require('./webservice/webService');
const port = process.env.PORT || 3000;

const modules = ['fibonacci'];
const webServer = new WebService(modules);

const instance = webServer.start(port);
module.exports = instance;