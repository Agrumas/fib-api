const {expect} = require('chai');
const request = require('supertest');
process.env.PORT = 3001;
const app = require('../bootstrap');

describe('/fibonacci', function () {

	describe('GET', function () {
		it('returns requested number of items', (done) => {
			request(app)
				.get('/fibonacci?n=5')
				.expect(200, '0 1 1 2 3', done);
		});

		it('returns zero numbers', (done) => {
			request(app)
				.get('/fibonacci?n=0')
				.expect(200, '', done);
		});

		it('returns one number', (done) => {
			request(app)
				.get('/fibonacci?n=1')
				.expect(200, '0', done);
		});

		it('returns valid sequence of 80 numbers', () => {
			return request(app)
				.get('/fibonacci?n=80')
				.expect(200)
				.then((response) => {
					const body = response.text;
					const lastPos = body.lastIndexOf(' ') + 1;
					const last = body.substr(lastPos);
					expect(last, 'Last number to be valid').to.equal('14472334024676221');
				});
		});

		describe('validations', function () {
			it('returns error when number parameter is negative', (done) => {
				request(app)
					.get('/fibonacci?n=-5')
					.expect(400, 'Number must be positive', done);
			});

			it('returns error when number parameter is not a number', (done) => {
				request(app)
					.get('/fibonacci?n=a')
					.expect(400, 'Number is not provided or it is invalid', done);
			});
			it('returns error when number parameter is float', (done) => {
				request(app)
					.get('/fibonacci?n=1.5')
					.expect(400, 'Number is not provided or it is invalid', done);
			});
		});
	});

	after(function () {
		app.close();
	});
});